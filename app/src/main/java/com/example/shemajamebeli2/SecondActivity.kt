package com.example.shemajamebeli2

import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.shemajamebeli2.databinding.ActivityMainBinding
import com.example.shemajamebeli2.databinding.ActivitySecondBinding

class SecondActivity:AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnBack.setOnClickListener {
            finish()
        }
        init()
    }
    private fun init(){
        binding.tvName.setText(intent.getStringExtra("Extra_Name"))
        binding.tvLastName.setText(intent.getStringExtra("Extra_LastName"))
        binding.tvAge.setText(intent.getIntExtra("Extra_Age",0))
        binding.tvEmail.setText(intent.getStringExtra("Extra_Email"))
    }
}